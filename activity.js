const HTTP = require('http');
const PORT = 3000;



//Get method


fetch(`https://jsonplaceholder.typicode.com/todos`)
.then(data => data.json())
.then(data => {
    data.forEach(element => {
        console.log(element)
    })
})

//single to do list

fetch(`https://jsonplaceholder.typicode.com/todos/50`)
.then(response => response.json())
.then(response => {
    console.log(response)
})

//status and title
fetch(`https://jsonplaceholder.typicode.com/todos/50`, {
    method: "PUT",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        title: "Wash dishes",
        status: "Done",
        userId: 50
    })
})
.then(response => response.json())
.then(response => {
    console.log(response)
})


//POST method
fetch(`https://jsonplaceholder.typicode.com/todos`, {
    method: "POST",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        title: "New to do item",
        body: "Cook dinner",
        userId: 1
    })
})
.then(response => response.json())
.then(response => {
    console.log(response)
})

//update a to do list item
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PUT",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        title: "Cook",
        body: "Halfway done",
        userId: 1
    })
})
.then(response => response.json())
.then(response => {
    console.log(response)
})

//change data structure
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PUT",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        title: "Cook",
        description: "Cook lunch",
        Status: "Done",
        dateCompleted: "03-07-2022",
        userId: 1
    })
})
.then(response => response.json())
.then(response => {
    console.log(response)
})

//Patch
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
    method: "PATCH",
    headers: {
        "Content-type": "application/json"
    },
    body: JSON.stringify({
        title: "Cook dinner next"
    })
})
.then(response => response.json())
.then(response => {
    console.log(response)
})

//delete
fetch(`https://jsonplaceholder.typicode.com/todos/2`,{
    method: "DELETE"
})



HTTP.createServer((req, res) => {
    if (req.url == "https://jsonplaceholder.typicode.com/todos" && method == "GET") {
        let reqBody = "";
        req.on("data", (data) => {
            reqBody += data
        })
        req.on("end", () => {
        reqBody = JSON.parse(reqBody)
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.write(JSON.stringify("https://jsonplaceholder.typicode.com/todos"))
        res.end()
        })
    }
}).listen(PORT, () => console.log(`Server connected to port ${PORT}`));


HTTP.createServer((req, res) => {
    if (req.url == "https://jsonplaceholder.typicode.com/todos/1" && method == "GET") {
        let reqBody = "";
        req.on("data", (data) => {
            reqBody += data
        })
        req.on("end", () => {
        reqBody = JSON.parse(reqBody)
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.write(JSON.stringify("https://jsonplaceholder.typicode.com/todos/1"))
        res.end()
        })
    }
}).listen(PORT, () => console.log(`Server connected to port ${PORT}`));



HTTP.createServer((req, res) => {
    if (req.url == "https://jsonplaceholder.typicode.com/todos" && method == "POST") {
        let reqBody = "";
        req.on("data", (data) => {
            reqBody += data
        })
        req.on("end", () => {
        reqBody = JSON.parse(reqBody)
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.write(JSON.stringify("https://jsonplaceholder.typicode.com/todos"))
        res.end()
        })
    }
}).listen(PORT, () => console.log(`Server connected to port ${PORT}`));



HTTP.createServer((req, res) => {
    if (req.url == "https://jsonplaceholder.typicode.com/todos/1" && method == "PUT") {
        let reqBody = "";
        req.on("data", (data) => {
            reqBody += data
        })
        req.on("end", () => {
        reqBody = JSON.parse(reqBody)
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.write(JSON.stringify("https://jsonplaceholder.typicode.com/todos/1"))
        res.end()
        })
    }
}).listen(PORT, () => console.log(`Server connected to port ${PORT}`));


HTTP.createServer((req, res) => {
    if (req.url == "https://jsonplaceholder.typicode.com/todos/1" && method == "PATCH") {
        let reqBody = "";
        req.on("data", (data) => {
            reqBody += data
        })
        req.on("end", () => {
        reqBody = JSON.parse(reqBody)
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.write(JSON.stringify("https://jsonplaceholder.typicode.com/todos/1"))
        res.end()
        })
    }
}).listen(PORT, () => console.log(`Server connected to port ${PORT}`));



HTTP.createServer((req, res) => {
    if (req.url == "https://jsonplaceholder.typicode.com/todos/1" && method == "DELETE") {
        let reqBody = "";
        req.on("data", (data) => {
            reqBody += data
        })
        req.on("end", () => {
        reqBody = JSON.parse(reqBody)
        res.writeHead(200, {"Content-Type": "text/plain"})
        res.write(JSON.stringify("https://jsonplaceholder.typicode.com/todos/1"))
        res.end()
        })
    }
}).listen(PORT, () => console.log(`Server connected to port ${PORT}`));